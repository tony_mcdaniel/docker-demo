FROM python:3.5

MAINTAINER Tony McDaniel <tony.mcdaniel@ctgcompanies.com>

RUN apt-get update && apt-get install -y --no-install-recommends \
nginx supervisor

RUN mkdir /config /code /logs

ADD ./config /config

RUN pip install -r /config/requirements.txt

RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default

RUN ln -s /config/nginx.conf /etc/nginx/sites-enabled/
RUN ln -s /config/supervisor.conf /etc/supervisor/conf.d/

ADD ./code /code

WORKDIR /code

EXPOSE 80

CMD ["supervisord", "-n"]
