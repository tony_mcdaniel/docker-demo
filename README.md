# README #

A quick overview of using Docker for development and production creating a simple Flask web app.

### What's covered? ###

* Flask 0.10 web app with Python 3.5
* Building and running a container
* Using docker-compose
* Production deployment with Nginx
* Secure methods for handling passwords

### What do I need? ###

* [Docker](https://www.docker.com/products/overview)
* PDF viewer to view slides

### Branches ###

The different versions have their own branches:

* [simple](https://bitbucket.org/tony_mcdaniel/docker-demo/branch/simple): Flask app with Dockerfile
* [compose](https://bitbucket.org/tony_mcdaniel/docker-demo/branch/compose): adds docker-compose.yml
* [supervisor](https://bitbucket.org/tony_mcdaniel/docker-demo/branch/supervisor): adds Nginx and Supervisord
* [secrets](https://bitbucket.org/tony_mcdaniel/docker-demo/branch/secrets): adds secrets.env